package de.stl.saar.internetentw1.utils;

import java.util.Random;


/**
 * Enthaelt Zugriffsmethoden fuer die Erzeugung von Zufallszahlen. Kapselt
 * somit die gesamte Erzeugung von Zufallszahlen.
 * @author christopher
 *
 */
public class RandomUtils {
	private static final Random random;
	
	static {
		random = new Random();
	}

    // function to generate a random string of length n
    public static String getAlphaNumericString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

	/**
	 * Erzeugt eine double-Zufallszahl von 0 bis zu einem Maximalwert.
	 * @param max Der Maximalwert.
	 * @return Die erzeugte Zufallszahl.
	 */
	public static double nextDouble(double max) {
		return random.nextDouble() * max;
	}
	
	/**
	 * Erzeugt eine int-Zufallszahl von 0 bis zu einem Maximalwert.
	 * @param max Der Maximalwert.
	 * @return Die erzeugte Zufallszahl.
	 */
	public static int nextInt(int max) {
		return random.nextInt(max);
	}
	
	public static int nextInt() {
		return random.nextInt();
	}
}