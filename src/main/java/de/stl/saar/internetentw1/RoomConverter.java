package de.stl.saar.internetentw1;

import de.stl.saar.internetentw1.model.Room;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named
@FacesConverter(forClass = Room.class)
public class RoomConverter implements Converter {

    private static final int LENGTH = 4;
    private final String WRONG_LENGTH = "Die von Ihnen eingegeben Raum Nummer ist zu kurz sie muss genau 4 Zeichen lang sein.";
    private final String BUILDING_SHORT = "Die Gebäudenummer (die erste Zahl der Raumnummer) muss zwischen 1 und 8 liegen.";
    private final String ETAGE_SHORT = "Die Etagennummer (Zweite Ziffer der Raumnummer) muss zwischen 0 und 2 liegen.   ";
    private final String ROOM_SHORT = "Die Raummnummer (letzen beiden Ziffern der Raummnummer) muss zwischen 1 und 20 leigen";
    private String building = "";
    private String etage = "";
    private String roomNumber = "";
    private String thirdChar = "";

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        Room room = null;
        if (s.length() != LENGTH) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, WRONG_LENGTH, WRONG_LENGTH);
            facesContext.addMessage(uiComponent.getClientId(), message);
        }
        building = s.substring(0, 1);
        etage = s.substring(1, 2);
        thirdChar = s.substring(2, 2);
        roomNumber = s.substring(2);
        int thirdCharNumb = Integer.parseInt(thirdChar);
        int roomNumb;
        if (thirdCharNumb != 0) {
            roomNumb = Integer.parseInt(roomNumber);
        } else {
            roomNumb = Integer.parseInt(s.substring(3));
        }
        int buildingNumber = Integer.parseInt(building);
        int eatageNumber = Integer.parseInt(etage);

        if (!(buildingNumber >= 1 && buildingNumber < 9)) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, BUILDING_SHORT, BUILDING_SHORT);
            facesContext.addMessage(uiComponent.getClientId(), message);
        }
        if (!(eatageNumber < 0 && eatageNumber < 3)) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ETAGE_SHORT, ETAGE_SHORT);
            facesContext.addMessage(uiComponent.getClientId(), message);
        }

        if (!(roomNumb >= 1 && roomNumb <= 20)) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ETAGE_SHORT, ETAGE_SHORT);
            facesContext.addMessage(uiComponent.getClientId(), message);
        }
        room = new Room(buildingNumber, eatageNumber, roomNumb);

        return room;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        StringBuilder roomNumber = new StringBuilder();
        if (o instanceof Room) {
            Room room = (Room) o;
            roomNumber.append(room.getBuilding()).append(room.getFloor());
            if (room.getRoom() > 9) {
                roomNumber.append(room.getRoom());
            } else {
                roomNumber.append(0).append(room.getRoom());
            }
        }
        return roomNumber.toString();
    }
}
