package de.stl.saar.internetentw1.view;

import de.stl.saar.internetentw1.dao.interfaces.RoleDao;
import de.stl.saar.internetentw1.i18n.Translator;
import de.stl.saar.internetentw1.model.User;
import de.stl.saar.internetentw1.utils.JsfUtils;
import org.primefaces.model.menu.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class MainNavigationView {

    @Inject
    private RoleDao roleDao;
    @Inject
    private Translator translator;

    private User currentUser;

    private MenuModel model;

    private boolean notOwnProfile;

    @PostConstruct
    public void initializeBean() {
        currentUser = (User) JsfUtils.getBeanAttribute("currentUser", "loginView", User.class);
        model = new DefaultMenuModel();
        setNotOwnProfile(true);
    }

    public void initialize(ComponentSystemEvent event) {
        setNotOwnProfile(true);
        if (currentUser != null) {
            if (roleDao.checkRole(currentUser, 1)) { //Role with id 1 is Admin
                buildModelAdmin();
            }
        }
        buildModelNormalUser();
    }

    private void buildModelAdmin() {
        Submenu submenu = new DefaultSubMenu(translator.get("subMenuAdmin"));
        DefaultMenuItem mealManagement = new DefaultMenuItem(translator.get("menuItemMealManagement"));
        mealManagement.setIcon("fa fa-fw fa-users");
        mealManagement.setCommand("#{mainNavigationView.onMealManagementSelected}");


        DefaultMenuItem userManagement = new DefaultMenuItem(translator.get("menuItemUserManagement"));
        userManagement.setIcon("fa fa-fw fa-users");
        userManagement.setCommand("#{mainNavigationView.onUserManagementSelected}");
        List<MenuElement> elementList = new ArrayList();
        elementList.add(mealManagement);
        elementList.add(userManagement);
        ((DefaultSubMenu) submenu).setElements(elementList);
        model.addElement(submenu);
    }

    private void buildModelNormalUser() {
        Submenu submenu = new DefaultSubMenu();
        DefaultMenuItem changeOwnProfile = new DefaultMenuItem(translator.get("menuItemChangeOwnProfile"));
        changeOwnProfile.setCommand("#{mainNavigationView.onChangeOwnProfileSelected}");
        changeOwnProfile.setIcon("fa fa-fw fa-user");

        DefaultMenuItem orders = new DefaultMenuItem(translator.get("menuItemOrder"));
        orders.setIcon("fa fa-fw fa-shopping-cart");
        orders.setCommand("#{mainNavigationView.onOrderSelected}");

        DefaultMenuItem logOut = new DefaultMenuItem(translator.get("menuItemLogOut"));
        logOut.setIcon("fa fa-fw fa-sign-out");
        logOut.setCommand("#{loginView.logOut}");
        List<MenuElement> elementList = new ArrayList();
        elementList.add(changeOwnProfile);
        elementList.add(orders);
        elementList.add(logOut);
        ((DefaultSubMenu) submenu).setElements(elementList);
        model.addElement(submenu);
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public String onMealManagementSelected() {
        return "dishManagementTable?faces-redirect=true";
    }

    public String onUserManagementSelected() {
        return "userManagementTable?faces-redirect=true";
    }

    public String onOrderSelected() {
        return "orders?faces-redirect=true";
    }

    public String onChangeOwnProfileSelected() {
        setNotOwnProfile(false);
        return "userAdd?faces-redirect=true";
    }

    public boolean isNotOwnProfile() {
        return notOwnProfile;
    }

    public void setNotOwnProfile(boolean notOwnProfile) {
        this.notOwnProfile = notOwnProfile;
    }
}
