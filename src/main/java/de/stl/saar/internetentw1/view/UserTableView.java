package de.stl.saar.internetentw1.view;

import de.stl.saar.internetentw1.dao.interfaces.UserDao;
import de.stl.saar.internetentw1.model.User;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class UserTableView {

    @Inject
    private UserDao userDao;

    private List<User> userList;
    private DataModel<User> userDataModel;
    private User currentUser;

    @PostConstruct
    public void initializeBean() {
    }

    public void initialize(ComponentSystemEvent event) {
        userList = userDao.findAllUsers();
        userDataModel = new ListDataModel(userList);
        currentUser = null;
    }

    public String editButton() {
        currentUser = userDataModel.getRowData();
        System.out.println(currentUser.getUserId());
        return "userAdd?faces-redirect=true";
    }

    public String deleteButton() {
        User selectedUser = userDataModel.getRowData();
        userDao.removeUser(selectedUser.getUserId());
        userList = userDao.findAllUsers();
        userDataModel = new ListDataModel<User>(userList);
        return "userManagementTable?faces-redirect=true";
    }

    public String backButton() {
        return "mainNavigation?faces-redirect=true";
    }

    public String addButton() {
        return "userAdd?faces-redirect=true";
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public DataModel<User> getUserDataModel() {
        return userDataModel;
    }

    public void setUserDataModel(DataModel<User> userDataModel) {
        this.userDataModel = userDataModel;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public int getNumber(User user) {
        return userList.indexOf(user) + 1;
    }
}


