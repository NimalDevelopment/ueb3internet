package de.stl.saar.internetentw1.view;


import de.stl.saar.internetentw1.model.Language;
import de.stl.saar.internetentw1.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Named
@SessionScoped
public class I18nBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String countryCode = "de";

    private List<Language> languages;

    @PostConstruct
    public void initialize() {
        languages = new ArrayList<Language>();
        languages.add(new Language(Locale.GERMAN, "de"));
        languages.add(new Language(Locale.ENGLISH, "en"));
        countryCode = Locale.GERMAN.getLanguage();
    }


    public void onLocaleCodeChange(String countryCode) {
        this.countryCode = countryCode;
        JsfUtils.setLocale(new Locale(countryCode));
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public void setLanguageGerman() {
        onLocaleCodeChange("de");
    }

    public void setLanguageEnglish() {
        onLocaleCodeChange("en");
    }

}