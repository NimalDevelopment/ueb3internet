package de.stl.saar.internetentw1.view;

import de.stl.saar.internetentw1.dao.interfaces.UserDao;
import de.stl.saar.internetentw1.i18n.Translator;
import de.stl.saar.internetentw1.model.User;
import de.stl.saar.internetentw1.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.util.List;

@Named
@SessionScoped
public class LoginView {
    @Inject
    private UserDao userDao;
    @Inject
    private Translator translator;
    private List<User> userList;
    private String username;
    private String password;
    private User currentUser;
    private String message;
    int i = 0;

    @PostConstruct
    public void initializeBean() {
        userList = userDao.findAllUsers();
        message = translator.get("labelLoginText");
    }

    public void initialize(ComponentSystemEvent event) {
        username = "";
        password = "";
    }

    public String login() {
        for (User user : userList) {
            if (user.getUsername().equals(username)) {
                if (user.getPassword().equals(password)) {
                    currentUser = user;
                    message = translator.get("labelLoginTexts");
                    return "mainNavigation?faces-redirect=true";
                }
            }
        }
        if (currentUser == null) {
            loginFail();
        }
        return null;
    }

    private String loginFail() {
        message = translator.get("labelTitleIncorrectPwd");
        return "login?faces-redirect=true";
    }

    private String loginSuccess() {
        return "mainNavigation?faces-redirect=true";
    }

    public String logOut() {
        currentUser = null;
        HttpSession session = (HttpSession) JsfUtils.getFacesContext().getExternalContext().getSession(false);
        session.invalidate();
        return "login?faces-redirect=true";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
