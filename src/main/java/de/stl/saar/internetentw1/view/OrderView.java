package de.stl.saar.internetentw1.view;

import de.stl.saar.internetentw1.dao.interfaces.DishDao;
import de.stl.saar.internetentw1.i18n.Translator;
import de.stl.saar.internetentw1.model.Dish;
import de.stl.saar.internetentw1.model.Room;
import de.stl.saar.internetentw1.utils.JsfUtils;
import org.primefaces.event.FlowEvent;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
@Named
public class OrderView {

    @Inject
    private DishDao dishDao;
    @Inject
    private Translator translator;

    private List<Dish> dishList;
    private DataModel<Dish> dishDataModel;
    private DataModel<Dish> basketDataModel;
    private List<Dish> basket;
    private Dish selectedDish;
    private double totalPrice;
    private Room room;
    private String oderFor;

    @PostConstruct
    public void initializeBean() {
        selectedDish = null;
        basket = new ArrayList();
        basket.clear();
        dishList = dishDao.findAllDishes();
        dishDataModel = new ListDataModel(dishList);
        basketDataModel = new ListDataModel(basket);
        totalPrice = 0;
    }

    public void initialize(ComponentSystemEvent event) {


    }

    public void addToBasket() {
        selectedDish = dishDataModel.getRowData();
        basket.add(selectedDish);
        totalPrice += selectedDish.getPrice();
    }

    public String deleteFromBasket() {
        selectedDish = basketDataModel.getRowData();
        basket.remove(selectedDish);
        if (basket.isEmpty()) {
            return "orders?faces-redirect=true";
        }
        return null;
    }

    public String makeOrder() {
        JsfUtils.addInfoMessageToContext(translator.get("labelMakeOrderHeader"), translator.get("labelMsgMakeOrder", room));
        return "orders?faces-redirect=true";
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    public DishDao getDishDao() {
        return dishDao;
    }

    public void setDishDao(DishDao dishDao) {
        this.dishDao = dishDao;
    }

    public List<Dish> getDishList() {
        return dishList;
    }

    public void setDishList(List<Dish> dishList) {
        this.dishList = dishList;
    }

    public DataModel<Dish> getDishDataModel() {
        return dishDataModel;
    }

    public void setDishDataModel(DataModel<Dish> dishDataModel) {
        this.dishDataModel = dishDataModel;
    }

    public List<Dish> getBasket() {
        return basket;
    }

    public DataModel<Dish> getBasketDataModel() {
        return basketDataModel;
    }

    public void setBasketDataModel(DataModel<Dish> basketDataModel) {
        this.basketDataModel = basketDataModel;
    }

    public void setBasket(List<Dish> basket) {
        this.basket = basket;
    }

    public int getNumber(Dish dish) {
        return dishList.indexOf(dish) + 1;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getOderFor() {
        return oderFor;
    }

    public void setOderFor(String oderFor) {
        this.oderFor = oderFor;
    }
}
