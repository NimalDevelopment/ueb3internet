package de.stl.saar.internetentw1.view;

import de.stl.saar.internetentw1.dao.interfaces.DishDao;
import de.stl.saar.internetentw1.model.Dish;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class DishTableView {

    @Inject
    private DishDao dishDao;

    private List<Dish> dishList;
    private DataModel<Dish> dishModel;
    private Dish currentDish;

    @PostConstruct
    public void initializeBean() {
        dishList = dishDao.findAllDishes();
        dishModel = new ListDataModel<Dish>(dishList);
    }

    public void initialize(ComponentSystemEvent event) {
        dishList = dishDao.findAllDishes();
        dishModel = new ListDataModel<Dish>(dishList);
        currentDish = null;
    }

    public String editButton() {
        currentDish = dishModel.getRowData();
        System.out.println(currentDish.getDishId());
        return "dishAdd?faces-redirect=true";
    }

    public String deleteButton() {
        Dish selectedDish = dishModel.getRowData();
        dishDao.removeDish(selectedDish.getDishId());
        dishList = dishDao.findAllDishes();
        dishModel = new ListDataModel<Dish>(dishList);
        return "dishManagementTable?faces-redirect=true";
    }

    public String backButton() {
        return "mainNavigation?faces-redirect=true";
    }

    public String addButton() {
        return "dishAdd?faces-redirect=true";
    }

    public int getNumber(Dish dish) {
        return dishList.indexOf(dish) + 1;
    }

    public List<Dish> getDishList() {
        return dishList;
    }

    public void setDishList(List<Dish> dishList) {
        this.dishList = dishList;
    }

    public DataModel<Dish> getDishModel() {
        return dishModel;
    }

    public void setDishModel(DataModel<Dish> dishModel) {
        this.dishModel = dishModel;
    }

    public Dish getCurrentDish() {
        return currentDish;
    }

    public void setCurrentDish(Dish currentDish) {
        this.currentDish = currentDish;
    }
}
