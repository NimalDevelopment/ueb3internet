package de.stl.saar.internetentw1.view;

import de.stl.saar.internetentw1.dao.interfaces.RoleDao;
import de.stl.saar.internetentw1.dao.interfaces.UserDao;
import de.stl.saar.internetentw1.model.Role;
import de.stl.saar.internetentw1.model.User;
import de.stl.saar.internetentw1.utils.JsfUtils;
import de.stl.saar.internetentw1.utils.RandomUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class AddUserView {
    @Inject
    private UserDao userDao;
    @Inject
    private RoleDao roleDao;
    private User user;
    private User currentUser;
    private Role role;
    private String username;
    private String password;
    private String email;
    private int userId;
    private List<Role> roles;
    private boolean checked;
    private boolean newPw;
    private Role rolename;
    private boolean toMenu;


    @PostConstruct
    public void initializeBean() {
        newPw = false;
    }

    public void initialize(ComponentSystemEvent event) {
        currentUser = (User) JsfUtils.getBeanAttribute("currentUser", "userTableView", User.class);
        roles = roleDao.findAllRoles();

        if (newPw == false) {
            setPassword("");
            setChecked(false);
            setEmail("");
            setUsername("");
        }
        setUserId(RandomUtils.nextInt());
        for (User u : userDao.findAllUsers()) {
            if (u.getUserId() == userId)
                setUserId(userId + 1);
        }
        setRoles(roleDao.findAllRoles());
        if (currentUser != null) {
            setUsername(currentUser.getUsername());
            if (!newPw)
                setPassword(currentUser.getPassword());
            setRole(currentUser.getRole());
            setEmail(currentUser.getEmail());
            setUserId(currentUser.getUserId());
            rolename = role;
            System.out.println(rolename.getRoleName());
        }
    }

    public String buttonCancel() {
        toMenu = (Boolean) JsfUtils.getBeanAttribute("notOwnProfile", "mainNavigationView", boolean.class);
        if (toMenu)
            return "userManagementTable?faces-redirect=true";
        else
            return "mainNavigation?faces-redirect=true";
    }

    public String buttonSave() {
        if (currentUser != null) {
            userDao.removeUser(currentUser.getUserId());
        }
        user = new User(userId, username, email, password, roleDao.findRoleByName(rolename.getRoleName()));
        userDao.addUser(user);
        currentUser = null;
        newPw = false;
        toMenu = (Boolean) JsfUtils.getBeanAttribute("notOwnProfile", "mainNavigationView", boolean.class);
        if (toMenu)
            return "userManagementTable?faces-redirect=true";
        else
            return "mainNavigation?faces-redirect=true";
    }

    public String buttonPw() {
        setChecked(true);
        String pw = RandomUtils.getAlphaNumericString(10);
        setPassword(pw);
        newPw = true;
        return "userAdd?faces-redirect=true";
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public RoleDao getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }


    public List<Role> getRoleValues() {
        return roles;
    }
    /*
    public SelectItem[] getRoleValues() {
        SelectItem[] items = new SelectItem[roles.size()];
        int i = 0;
        for (Role c : roles) {
            items[i++] = new SelectItem(c, c.getRoleName());
        }
        return items;
    }*/

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isNewPw() {
        return newPw;
    }

    public void setNewPw(boolean newPw) {
        this.newPw = newPw;
    }

    public Role getRolename() {
        return rolename;
    }

    public void setRolename(Role rolename) {
        this.rolename = rolename;
    }
}
