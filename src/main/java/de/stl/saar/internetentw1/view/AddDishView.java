package de.stl.saar.internetentw1.view;

import de.stl.saar.internetentw1.dao.interfaces.DishDao;
import de.stl.saar.internetentw1.model.Category;
import de.stl.saar.internetentw1.model.Dish;
import de.stl.saar.internetentw1.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class AddDishView {
    @Inject
    private DishDao dishDao;
    private Category category;
    private Dish dish;
    private Dish currentDish;
    private double price;
    private String name;


    @PostConstruct
    public void initializeBean() {

    }

    public void initialize(ComponentSystemEvent event) {
        currentDish = (Dish) JsfUtils.getBeanAttribute("currentDish", "dishTableView", Dish.class);

        setPrice(0.0);
        setName("");

        if (currentDish != null) {
            setPrice(currentDish.getPrice());
            setName(currentDish.getDishName());
            setCategory(currentDish.getCategory());
        }
    }


    public String buttonSave() {
        if (currentDish != null)
            dishDao.removeDish(currentDish.getDishId());
        dish = new Dish(name, price, category, "");
        dishDao.addDish(dish);
        currentDish = null;
        return "dishManagementTable?faces-redirect=true";
    }


    public SelectItem[] getCategoryValues() {
        SelectItem[] items = new SelectItem[Category.values().length];
        int i = 0;
        for (Category c : Category.values()) {
            items[i++] = new SelectItem(c, c.getCategoryName());
        }
        return items;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DishDao getDishDao() {
        return dishDao;
    }

    public void setDishDao(DishDao dishDao) {
        this.dishDao = dishDao;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

}
