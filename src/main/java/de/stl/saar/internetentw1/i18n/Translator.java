package de.stl.saar.internetentw1.i18n;

public interface Translator {

    String get(String key);

    String get(String key, Object... objects);

}
