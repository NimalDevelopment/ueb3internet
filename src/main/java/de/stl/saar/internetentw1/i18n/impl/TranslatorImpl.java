package de.stl.saar.internetentw1.i18n.impl;

import de.stl.saar.internetentw1.i18n.I18nUtil;
import de.stl.saar.internetentw1.i18n.Translator;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.text.MessageFormat;
import java.util.MissingResourceException;

@SessionScoped
@Named
public class TranslatorImpl implements Translator {

    @Override
    public String get(String key) {
        String translation;
        try {
            translation = I18nUtil.getComponentLabelsResourceBundle().getString(key);
        } catch (MissingResourceException e) {
            translation = "???_" + key + "_???";
        }
        return translation;
    }

    @Override
    public String get(String key, Object... objects) {
        String translation;
        try {
            MessageFormat formatter = new MessageFormat("");
            formatter.applyPattern(I18nUtil.getComponentLabelsResourceBundle().getString(key));
            translation = formatter.format(objects);

        } catch (MissingResourceException e) {
            translation = "???_" + key + "_???";
        }
        return translation;
    }
}
