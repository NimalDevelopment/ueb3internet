package de.stl.saar.internetentw1.i18n;

import java.util.ResourceBundle;

/**
 * Encapsulates the resource bundles. Only the other classes in this package
 * are allowed to call the methods of this class. Therefore it is only
 * package public.
 *
 * @author Christopher Olbertz
 */
public class I18nUtil {
    private static final String I18N_BASENAME_COMPONENT_LABELS = "i18n/labels";

    private static ResourceBundle resourceBundleComponentLabels;

    static {
        resourceBundleComponentLabels = ResourceBundle.getBundle(I18N_BASENAME_COMPONENT_LABELS);
    }

    public static ResourceBundle getComponentLabelsResourceBundle() {
        return resourceBundleComponentLabels;
    }
}