package de.stl.saar.internetentw1.dao.classes;

import de.stl.saar.internetentw1.dao.interfaces.RoleDao;
import de.stl.saar.internetentw1.model.Role;
import de.stl.saar.internetentw1.model.User;
import de.stl.saar.internetentw1.utils.RandomUtils;
import de.stl.saar.internetentw1.utils.StringUtils;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.*;

/**
 * Simuliert die Role-Tabelle einer Datenbank mithilfe
 * einer Map, wobei der Primärschlüssel der Schlüssel der
 * Map ist.
 *
 * @author christopher
 */
@Named
@SessionScoped
public class RoleDaoImpl implements RoleDao {
    private Map<Integer, Role> roleTable;

    public RoleDaoImpl() {
        roleTable = new HashMap();
        Role role1 = new Role(1, "admin");
        Role role2 = new Role(2, "user");
        addRole(role1);
        addRole(role2);
    }

    @Override
    public void addRole(Role role) {
        int primaryKeyValue = role.getRoleId();

        if (primaryKeyValue > 0) {
            if (!primaryKeyValueFree(primaryKeyValue)) {
                primaryKeyValue = createPrimaryKeyValue();
            }
        } else {
            primaryKeyValue = createPrimaryKeyValue();
            role.setRoleId(primaryKeyValue);
        }

        roleTable.put(primaryKeyValue, role);
    }

    /**
     * Erzeugt einen neuen Primärschlüsselwert. Dabei wird der Wert
     * zufällig erzeugt und dann wird geprüft, ob es bereits einen
     * Datensatz mit diesem Wert gibt. Falls nein, wird dieser
     * Primärschlüsselwert zurückgegeben.
     *
     * @return Der neu erzeugte und noch nicht vergebene Primärschlüsselwert.
     */
    private int createPrimaryKeyValue() {
        int primaryKey = 0;

        do {
            primaryKey = RandomUtils.nextInt();
        } while (!primaryKeyValueFree(primaryKey));

        return primaryKey;
    }

    /**
     * Prüft, ob ein Primärschlüsselwert bereits vergeben ist.
     *
     * @param primaryKeyValue Der zu prüfende Wert.
     * @return True, falls der Wert bereits als Primärschlüsselwert in
     * der Tabelle vorkommt, sonst false.
     */
    private boolean primaryKeyValueFree(int primaryKeyValue) {
        Role roleWithPrimaryKey = roleTable.get(primaryKeyValue);
        if (roleWithPrimaryKey == null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void removeRole(int roleId) {
        roleTable.remove(roleId);
    }

    @Override
    public List<Role> findAllRoles() {
        Collection<Role> roleCollection = roleTable.values();
        List<Role> roles = new ArrayList(roleCollection);
        return roles;
    }

    @Override
    public Role findRoleByName(String roleName) {
        List<Role> roleList = findAllRoles();
        for (Role role : roleList) {
            boolean roleNameFound = StringUtils.areStringsEqual(roleName, role.getRoleName());
            if (roleNameFound) {
                return role;
            }
        }

        return null;
    }

    @Override
	public boolean checkRole(User user, int roleId) {
        return user.getRole().getRoleId() == roleId;
    }
}
